This project is to convert Beagle Bone code to code on RPI3. The code is to control speed of the DC motor based on PID.
PID parameters are made by the A/D converted data of IR sensor.
A/D converted data are got from MCP3008.

1.Wiring

The MCP3008 connects to the Raspberry Pi using a SPI serial connection.  You can use either the hardware SPI bus, or any four GPIO pins and software SPI to talk to the MCP3008.  Software SPI is a little more flexible since it can work with any pins on the Pi, whereas hardware SPI is slightly faster but less flexible because it only works with specific pins.

  -Software SPI
  
  To connect the MCP3008 to the Raspberry Pi with a software SPI connection you need to make the following connections:
	  
	  MCP3008 VDD to Raspberry Pi 3.3V
	  MCP3008 VREF to Raspberry Pi 3.3V
	  MCP3008 AGND to Raspberry Pi GND
	  MCP3008 DGND to Raspberry Pi GND
	  MCP3008 CLK to Raspberry Pi pin 18
	  MCP3008 DOUT to Raspberry Pi pin 23
	  MCP3008 DIN to Raspberry Pi pin 24
	  MCP3008 CS/SHDN to Raspberry Pi pin 25
	  
  -Hardware SPI
  
  To use hardware SPI first make sure you've enabled SPI using the raspi-config tool.  
  
      https://www.raspberrypi-spy.co.uk/2014/08/enabling-the-spi-interface-on-the-raspberry-pi/
	  
  Be sure to answer yes to both enabling the SPI interface and loading the SPI kernel module, then reboot the Pi.   
  Now wire the MCP3008 to the Raspberry Pi as follows:
	  
	  MCP3008 VDD to Raspberry Pi 3.3V
	  MCP3008 VREF to Raspberry Pi 3.3V
	  MCP3008 AGND to Raspberry Pi GND
	  MCP3008 DGND to Raspberry Pi GND
	  MCP3008 CLK to Raspberry Pi SCLK
	  MCP3008 DOUT to Raspberry Pi MISO
	  MCP3008 DIN to Raspberry Pi MOSI
	  MCP3008 CS/SHDN to Raspberry Pi CE0	

2.Library Install

You can install the library from the Python package index with a few commands, or you can install the library from its source on GitHub.  Pick one of these options below.  If you aren't sure I recommend installing from source on GitHub because it will also download examples to use the library.

	  https://github.com/adafruit/Adafruit_Python_MCP3008

To install from the source on Github connect to a terminal on the Raspberry Pi and run the following commands:

	  sudo apt-get update
	  sudo apt-get install build-essential python-dev python-smbus git
	  cd ~
	  git clone https://github.com/adafruit/Adafruit_Python_MCP3008.git
	  cd Adafruit_Python_MCP3008
	  sudo python setup.py install

3.Install Python Module RPi.GPIO.

      sudo apt-get install python-dev python-rpi.gpio
	
	




     
