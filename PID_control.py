#!/usr/bin/env python
import time

# import Adafruit_GPIO.SPI as SPI   # Import SPI library. (for hardware SPI)
import Adafruit_MCP3008             # Import MCP3008 library.
import RPi.GPIO as GPIO


# Hardware SPI configuration
# SPI_PORT   = 0
# SPI_DEVICE = 0
# mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

# Software SPI configuration
CLK = 18
MISO = 23
MOSI = 24
CS = 25
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)

# define pins of RPI3 connected to motor driver chip
MotorPinA1 = 2     # GPIO2(pin connected to chip's inputA1 pin)
MotorPinA2 = 3     # GPIO3(pin connected to chip's inputA2 pin)
MotorEnable1 = 4   # GPIO4(pin connected to chip's enable1 pin)
MotorPinB1 = 17    # GPIO17(pin connected to chip's inputB1 pin)
MotorPinB2 = 27    # GPIO27(pin connected to chip's inputB2 pin)
MotorEnable2 = 22  # GPIO22(pin connected to chip's enable2 pin)

# define global variables for PID control
integral = 0
last_prop = 0
Kp = 20
Ki = 0
Kd = 150
amax = list(0 for i in range(0, 8))
amin = list(1024 for i in range(0, 8))
timeout = time.time() + 10


def setup():
    """
    Setup RPI3's pins connected to motor driver chip.
    :return:
    """
    GPIO.setmode(GPIO.BCM)      # define style of pin number as GPIO number
    GPIO.setup(MotorPinA1, GPIO.OUT)
    GPIO.setup(MotorPinA2, GPIO.OUT)
    GPIO.setup(MotorEnable1, GPIO.OUT)
    GPIO.setup(MotorPinB1, GPIO.OUT)
    GPIO.setup(MotorPinB2, GPIO.OUT)
    GPIO.setup(MotorEnable2, GPIO.OUT)

    GPIO.output(MotorPinA1, GPIO.HIGH)    # motor1
    GPIO.output(MotorPinA2, GPIO.LOW)
    GPIO.output(MotorEnable1, GPIO.HIGH)  # enable the turning of the motor1
    GPIO.output(MotorPinB1, GPIO.LOW)     # motor2
    GPIO.output(MotorPinB2, GPIO.HIGH)
    GPIO.output(MotorEnable2, GPIO.HIGH)  # enable the turning of the motor2

    pwm1 = GPIO.PWM(04, 100)  # configuring Enable pin means GPIO-04 for PWM
    pwm2 = GPIO.PWM(22, 100)  # the frequency of pwm is 100Hz
    pwm1.start(0)             # starting it with 0% duty cycle
    pwm2.start(0)

def calibrate():
    """
    Calibrate the sensors reading to fit any given line
    :return:
    """
    global amin
    global amax

    while time.time() < timeout:
        for i in range(0, 8):
            amin[i] = min(amin[i], read_sensors()[i])
            amax[i] = max(amax[i], read_sensors()[i])
            GPIO.output(MotorEnable1, GPIO.LOW)
            GPIO.output(MotorEnable2, GPIO.LOW)
    GPIO.output(MotorEnable1, GPIO.HIGH)
    GPIO.output(MotorEnable2, GPIO.HIGH)


def read_sensors():
    """
    reading the all of the data A/D converted on the each channel
    :return:
    """
    print "Now is reading the data A/D converted on all of the channel..."

    sensors = []
    for i in range(8):
        sensors.append(adc_read(i))
    print sensors

    return sensors


def adc_read(ch):
    """
    Read the A/D converted data from the each channel of MCP3008 via SPI port.
    :param ch:
    :return:
    """
    data = mcp.read_adc(ch)
    time.sleep(0.5)

    return data


def calc_setpoint(x, y):
    """
    Calculate the setpoint.
    :param x:
    :param y:
    :return:
    """
    avg = 0
    sum = 0
    for i in range(8):
        avg += (x[i] - y[i]) * i * 100
        sum += x[i] - y[i]

    return avg / sum


def get_position(s_data):
    """
    Get the current position from the sensors data.
    :param s_data:
    :return:
    """
    return sensor_average(s_data)/sensor_sum(s_data)


def sensor_average(s_data):
    """
    Calculate the average value from the sensors data.
    :param s_data:
    :return:
    """
    avg = 0
    for i in range(8):
        avg += s_data[i]*i*100

    return avg


def sensor_sum(s_data):
    """
    Calculate the sum value from the sensors data.
    :param s_data:
    :return:
    """
    sum = 0
    for i in range(8):
        sum += s_data[i]
    return sum


def calc_pid(x, sp):
    """
    Calculate the error from PID controller.
    :param x:
    :param sp:
    :return:
    """
    global integral, last_prop, Kp, Ki, Kd

    pos = sensor_average(x)/sensor_sum(x)    # pos = position
    prop = pos - sp                          # prop = position - setpoint
    integral = integral + prop
    deriv = prop - last_prop
    last_prop = prop
    error = (prop*Kp + integral*Ki + deriv*Kd)/100

    return error


def calc_speed(error):
    """
    calculating the correspondent speed.
    :param error:
    :return:
    """
    avg_speed = 150
    # min = 100
    # pos = get_position(read_sensors())
    speed = []
    if error < -20:
        right = avg_speed - error
        left = avg_speed + error
    elif error > 20:
        right = avg_speed - error       # ???????????????
        left = avg_speed + error        # ???????????????
    else:
        right = avg_speed - error       # ???????????????
        left = avg_speed + error        # ???????????????
    speed.append(right)
    speed.append(left)

    return speed


def get_sensor(x):
    """
    Get the sensor data of specified channel.
    :param x:
    :return:
    """
    j = read_sensors()[x]

    return j


def destroy():
    """
    When 'ctrl+c' is pressed, motor is stopped.
    :return:
    """
    GPIO.output(MotorEnable1, GPIO.LOW)    # stop motor
    GPIO.output(MotorEnable2, GPIO.LOW)
    GPIO.cleanup()


if __name__ == '__main__':

    setup()
    try:
        while True:
            calibrate()
            s = read_sensors()
            setpoint = calc_setpoint(amax, amin)
            position = get_position(s)
            err = calc_pid(s, setpoint)
            print err

            # print "divided by 100:"
            speeds = calc_speed(err)
            print speeds
            right_motor = speeds[0]
            left_motor = speeds[1]
            pwm1.ChangeDutyCycle(right_motor)
            pwm2.ChangeDutyCycle(left_motor)
    except KeyboardInterrupt:
        destroy()
